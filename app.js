// Basic Express Server Setup
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes.js")
const courseRoutes = require("./routes/courseRoutes.js")

const app = express();
const port = process.env.PORT || 4000;


// Middlewares
// Allows our frontend app to access our backend app.
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// Main URI
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// Mongoose Connection Setup
mongoose.connect("mongodb+srv://aljhnprcno:admin123@batch224-percano.l8zics7.mongodb.net/couse-booking-API?retryWrites=true&w=majority", 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);

let db = mongoose.connection;

db.on("error", () => console.error("Connection Error."));
db.once("open", () => console.log("Connected to MongoDB."));







app.listen(port, () => {console.log(`API is now running at port: ${port}`)});