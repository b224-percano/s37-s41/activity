const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth.js");


// Routes
// Route for creating a courses
router.post("/addCourse", (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	courseController.addCourse(req.body, userData).then(resultFromController => res.send(resultFromController))
});


// Route for retrieving all the courses
router.get("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	courseController.getAllCourses(userData).then(resultFromController => res.send(resultFromController))
});


// Route for retrieving all active courses
router.get("/", (req,res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
});


// Route for retrieving a specific courses
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId)

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
});


// Route for updating a specific courses
router.put("/:courseId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	
	if(userData.isAdmin){
		courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send({auth: "Failed"})
	}
});


// Route for archiving a specific courses
router.put("/archive/:courseId", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){
		courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send (resultFromController))
	} else {
		res.send({auth: "Failed"})
	}
});




module.exports = router;