const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController.js');
const auth = require("../auth.js");



// Routes

// Route for Checking Email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});


// Route for user Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});


// Route for user Log In
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for user Details
// My answer on activity
/*router.post("/details", (req, res) => {
	userController.getProfile(req.body).then((resultFromController) => res.send(resultFromController)) 
});*/

router.post("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData)
	console.log(req.headers.authorization)

	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});


router.post("/enroll", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

		if(userData.isAdmin == false){
			let data = { userId: userData.id, 
						courseId: req.body.courseId 
			}

			userController.enroll(data).then(resultFromController => res.send(resultFromController))
			
		} else {
			return res.send("Administrators are unable to enroll.")
		}
});



module.exports = router;