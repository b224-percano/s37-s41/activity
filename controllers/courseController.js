const Course = require("../models/Course.js");


// Controller Functions

// Creating a new course
module.exports.addCourse = (reqBody, userData) => {

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	if(userData.isAdmin !== true){
		return false
	}else {
		return newCourse.save().then((course, error) => {
			if(error){
				return false
			}else {
				return true
			}
		})
	}
};


// Retrieving All Courses
module.exports.getAllCourses = (data) => {

	if(data.isAdmin) {
		return Course.find({}).then(result => {
			return result
		})
	} else {
		return false // "You are not an Admin"
	}
};


// Retrieve All Active Courses
module.exports.getAllActive = () => { 
	return Course.find({isActive: true}).then(result => {
		return result
	})
};


// Retrieve a Specific Course
module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result
	})
};


// Update a Specific Course
module.exports.updateCourse = (reqParams, reqBody) => {

	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((updateCourse, error) => {

		console.log(updateCourse)
		if(error) {
			return false
		} else {
			return true
		}
	})
};


// Archive a Specific Course
module.exports.archiveCourse = (reqParams, reqBody) => {
		
		let archivedCourse = {
			isActive: reqBody.isActive
		}

			return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((result,error) => {
				if (error){
					return false
				}else {
					return true
				}
			})
};



